####################
# Fonction pour l'utilisation de l'algorithme Backpack

### 1er cas
## [weight,value]
# T=[[1,3],[2,4],[3,5],[5,6],[9,8]]
# W=11
### return le tableau de possibilité
### return le resultat 
#W=weightMax / T=tableau des objet [weight,value]

def AlgoBagPackCompute(W,T) :
     n = len(T)
     Tab = [ ]
     K = [0] * (W+1)
     Tab.append(K[:])
     for j in range(n) :
          (wj,vj) = T[j]
          for w in range(wj,W+1) :
               K[w] = max(Tab[-1][w],Tab[-1][w-wj]+vj)
          Tab.append(K[:])
     return K[W],Tab


### on crée un tableau K=[]
### on exectute la 1er algo
# K,Tab = SaDsansrepet(W,T)
### On utilise K et Tab dans le 2eme algo pour avec la solution
def AlgoBagPackResult(W,Tab,T) :
     n = len(T)
     S = [False] * n
     v = Tab[n][W]
     w = W
     for j in reversed(range(n)) :
          (wj,vj) = T[j]
          if (wj <= w) and (v-vj == Tab[j][w-wj]) :
               S[j] = True
               v = v-vj
               w = w-wj
     return S
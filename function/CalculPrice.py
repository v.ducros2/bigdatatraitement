################################
# Fonction pour le calcul du du prix pour un people


def price(pricePeople,show,priceId):
     if pricePeople>show["prices"][priceId] or pricePeople==0:
           pricePeople=show["prices"][priceId]
           return math.ceil(pricePeople)
     else :
          return math.ceil(pricePeople)

def priceOfPeople(data, people, show):
     pricePeople=0

     #calcul grille tarifaire taille 3
     if len(show["prices"]) ==3:
          if people["age"] > 14:
               pricePeople=price(pricePeople,show,0)
          if people["age"] < 14:
               pricePeople=price(pricePeople,show,1)
          if len(data["people"])>12:
               pricePeople=price(pricePeople,show,2)
     
     #calcul grille tarifaire taille 6
     if len(show["prices"]) ==6:
          if 0 in people["choices"]:
               pricePeople=price(pricePeople,show,0)
          if 1 in people["choices"]:
               pricePeople=price(pricePeople,show,1)
          if 2 in people["choices"]:
               pricePeople=price(pricePeople,show,2)
          if people["age"] < 18 and 3 in people["choices"]:
               pricePeople=price(pricePeople,show,3)
          if 4 in people["choices"]:
               pricePeople=price(pricePeople,show,4)
          if 5 in people["choices"]:
               pricePeople=price(pricePeople,show,5)

     return pricePeople
# BigDataTraitement

## Mise en place de l'environement de travail
Pour ce faire il faut python en version 3,7,2 (derniére version)

### Paramétrage de l'environement
pip install virtualenv
Utiliser la commande suivante sur le dossier git
virtualenv bigdatatraitement

Activation de l'environement en lancant le bat qui est dans 
/bigdatatraitement\Scripts> .\activate.bat

### Installation des differents modules
pip install flask flask-jsonpify flask-sqlalchemy flask-restful pymongo jsonify dnspython pyshark


### Retour du pip freeze pour information sur les version des modules
pip freeze :
aniso8601==8.0.0
astroid==2.2.3
Click==7.0
colorama==0.4.1
dnspython==1.16.0
Flask==1.1.1
Flask-Cors==3.0.8
Flask-Jsonpify==1.5.0
Flask-RESTful==0.3.7
Flask-SQLAlchemy==2.4.1
isort==4.3.10
itsdangerous==1.1.0
Jinja2==2.10.3
jsonify==0.5
lazy-object-proxy==1.3.1
MarkupSafe==1.1.1
mccabe==0.6.1
py4j==0.10.7
pylint==2.3.1
pymongo==3.9.0
pyspark==2.4.4
pytz==2019.3
six==1.12.0
SQLAlchemy==1.3.11
typed-ast==1.3.1
virtualenv==16.7.7
Werkzeug==0.16.0
wrapt==1.11.1

### Pour passer en mode debbug

Cela permet de ne pas avoir besoin de relancer le serveur lors de modification

dans powershell :
$env:FLASK_ENV = "development"

dans CMD : 
set FLASK_ENV=development

sous Linux Mac :
export FLASK_APP=myapp
export FLASK_ENV=development
flask run

### Fonctionnement de GIT

On ne travail pas sur master
Il faut crée ca branch derivée de master
On commit sur ca branch MERCI de faire des messages de commit concis et claire en anglais !

Quand on veut ajouter son travaille sur la branch master:
il faut rebase sa branche par rapport a master
puis merge sa branche sur master

si quelqu'un a besoin d'aide dans le groupe, par exemple pour la résolution des conflits etc, demandez moi il ne faut pas push nimporte comment, MERCI pour le repo

### Connection a la base de donnée en ligne de commande
mongo "mongodb+srv://cluster0-rmte3.mongodb.net/admin"  --username root
mdp: rootroot

### Commande pour refaire la collection

on regarde les bases 
show dbs

on se connecte a une base
use Avignon

pour vider la collection
db.spectables.remove({})

pour faire l'insert il faut copier coller les objets a ajouter entre crochet
db.spectables.insertMany([])

### fonction dans postman
#### fonction recomendation
curl -X POST \
  http://127.0.0.1:8080/api/recommendation \
{
    "budget": 400,
    "address": "3 impasse charles fourrier, Toulouse",
    "types": [
      "cinéma",
      "danse",
      "théatre"
    ],
    "transports": [
      "voiture",
      "bus"
    ],
    "people": [
      {
        "username": "toto",
        "choices": [1, 4, 5],
        "age": 18
      },
      {
        "username": "tata",
        "choices": [4, 5],
        "age": 10
      },
      {
        "username": "titi",
        "choices": [0],
        "age": 24
      }
    ]
  }'


### requete utilisé pour avoir la liste des categories pour l'application
#### requete qui retourne toutes les categorie
curl -X GET \
  http://127.0.0.1:8080/api/categories \

### requete pour les tests
#### requete qui retourne toute la base
curl -X GET \
  http://127.0.0.1:8080/getDatabase \

### requete pour les tests
#### requete qui retourne les prix d'un spéctacle
curl -X GET \
  http://127.0.0.1:8080/api/prices \
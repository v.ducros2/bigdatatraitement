from flask import Flask, request
from flask_restful import Resource, Api
from flask import jsonify

from datetime import date
from bson import json_util

from function.AlgoBackPack import *
from function.AlgoCircuit import *
from function.CalculPrice import *

import math
import json
import pymongo

# ajout de cors pour la communication application
from flask_cors import CORS


app = Flask(__name__)
api = Api(app)
CORS(app)

try:
     client = pymongo.MongoClient("mongodb+srv://root:rootroot@cluster0-rmte3.mongodb.net/test?retryWrites=true&w=majority")
     client.server_info()
except pymongo.errors.ServerSelectionTimeoutError as err:
    print(err)

db = client.avignon


# route pour le debug liste la totalité de la base
@app.route('/getDatabase', methods=['GET'])
def apiAll():
     result= list(db.shows.find().limit(2))     
     return json.dumps(result, default=json_util.default)

# route pour recupére la liste des catégories
@app.route('/api/categories', methods=['GET'])
def makeformSearch():
     allCategories= list(db.shows.distinct("types"))
     for categorie in allCategories :
          if categorie == "" :
               allCategories.remove(categorie)
     return json.dumps(allCategories, default=json_util.default)

@app.route('/api/recommendation', methods=['POST'])
def makeRecommendation():
     #initilidation des tableaux
     tabShowWeightPrices=[]
     tabShowWeightPricesTmp=[]
     k=[]
     tab=[]
     circuitTab = []

     #recuperation de json d'envoie
     data = request.json

     #recupération de la liste des choix de catégorie
     categorieChoice = data["types"]
     #recuperation de budget
     budget=data["budget"]

     # recupération de la liste des show
     showsList = list (db.shows.find())

     # on instancie showsList pour conserver le find propre
     showsListPrice = list (db.shows.find())

     #pour chaque show
     for show in showsListPrice:
          #on recupére la liste de ses categorie
          categoriesListe = show["types"]

          # si la taille du tableau de prix est supérieur a 6 on prend un seul cas
          if len(show["prices"]) > 6:
               show["prices"]=show["prices"][0:6]


          #on calcul le prix
          totalPrice=0
          for people in data["people"]:
               pricePeople = 0
               # calcul le prix pour un people
               pricePeople = priceOfPeople(data,people,show)
               
               # calcul du prix total pour le groupe
               totalPrice=totalPrice+pricePeople
          show["prices"]=totalPrice
                    
          #pour chaque categorie dans la liste
          count=0
          weight=0
          for categorie in categoriesListe:
               #on compte le nombre de de corespondance
               if categorie in categorieChoice:
                    count=count+1

          if count == 0:
               weight=-1
          elif count == 1:
               weight=3
          elif count == 2:
               weight=5
          elif count >=3:
               weight=8

          tabShowWeightPrices.append([show["prices"],weight,show["_id"],show["position"]["geometry"]["coordinates"]])
          tabShowWeightPricesTmp.append([show["prices"],weight])

     #execution des algo
     k,tab = AlgoBagPackCompute(budget,tabShowWeightPricesTmp)
     result = AlgoBagPackResult(budget,tab,tabShowWeightPricesTmp)

     # on le fait en deux temps pour ne pas modifier l'index a cahque suppression
     # 1/ si le show n'est pas utilisé on le met a 0
    
     for i in range(len(result)):
          if result[i] == False:
               tabShowWeightPrices[i] = 0

     # 2/ si le show est a 0 on le supprime
     for i in range(len(tabShowWeightPrices)):
          if 0 in tabShowWeightPrices :
               tabShowWeightPrices.remove(0)

     # consrtuction du tableau pour l'algo du voyageur
     showCircuit = []
     for i in range(len(tabShowWeightPrices)):
          # [longtude,latitude,id]
          showCircuit.append([tabShowWeightPrices[i][3][0],tabShowWeightPrices[i][3][1],tabShowWeightPrices[i][2]])


     gc = GestionnaireCircuit()

     # On crée les villes fonction du tableau
     for i in range(len(showCircuit)):
          ville1 = Ville(showCircuit[i][0], showCircuit[i][1], showCircuit[i][2])
          gc.ajouterVille(ville1)
     
     pop = Population(gc, 50, True)

     ga = GA(gc)
     pop = ga.evoluerPopulation(pop)
     for i in range(0, 100):
          pop = ga.evoluerPopulation(pop)

     meilleurePopulation = pop.getFittest()

     for ville in meilleurePopulation.circuit:
          circuitTab.append([ville.lon,ville.lat,ville.nom])

     circuitTabFull = []
     for ville in circuitTab :
          for show in showsList :
               if ville[2]==show["_id"] :
                    circuitTabFull.append(show)

     showRenderTab = []
     tripRenderTab = []
     for i in range(len(circuitTabFull)) :

          tabPeoplePrice = []
          for people in data["people"]:
               pricePeople=0
               pricePeople = priceOfPeople(data,people,circuitTabFull[i])

               peoplePrice = {
                    "username": people["username"],
				"prices": [pricePeople]
               } 
               tabPeoplePrice.append(peoplePrice)
          
          showRender = {
			"index": i,
			"name": circuitTabFull[i]['name'],
			"time": circuitTabFull[i]['schedules'][0],
			"description": circuitTabFull[i]['description'],
			"image": circuitTabFull[i]['imageUrl'],
			"categories": circuitTabFull[i]['types'],
			"people": tabPeoplePrice
		}

          tripRender = {
			"index": i,
			"coordonates": {
				"lat": circuitTabFull[i]["position"]["geometry"]["coordinates"][0],
				"long": circuitTabFull[i]["position"]["geometry"]["coordinates"][1]
			},
			"address": circuitTabFull[i]["postalAddress"],
			"transport": "Moyen de transport pour se rendre à partir du lieu précédent"
		}
          showRenderTab.append(showRender)
          tripRenderTab.append(tripRender) 
     
     renderFinal = []
     last = {
          "events":showRenderTab,
          "trip":tripRenderTab
     }
     
     #tabShowWeightPrices la liste des spectale qui rentre dans le budget total fonction du poids
     return json.dumps(last, default=json_util.default)

@app.route('/api/prices', methods=['GET'])
def returnAllPrices():
     tab=[]
     showsList = list (db.shows.find())
     for show in showsList:
          tab.append(show['prices'])
     
     if len(showsList[0]['prices']) > 6:
          showsList[0]['prices']=showsList[0]['prices'][0:6]
     show = showsList[0]['prices']
     return json.dumps(show, default=json_util.default)

if __name__ == '__main__':
     app.run(host='0.0.0.0',port='8080')